{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}

module Slack (API, api) where

import Data.Aeson
import Data.Aeson.Types (Pair)
import Data.Monoid ((<>))
import qualified Data.Text as T
import Data.Text (Text, strip, intercalate)
import Data.Maybe (catMaybes)
import Spells
import Servant
import Web.FormUrlEncoded (FromForm, fromForm, parseUnique)

type API
  =  ReqBody '[FormUrlEncoded] PostData
  :> Post '[JSON] SlackResponse

newtype UserId = UserId String
  deriving (Show)
newtype Cmd = Cmd Text
  deriving (Show)

data PostData = PostData Cmd UserId
  deriving (Show)

instance FromForm PostData where
  fromForm f = PostData
    <$> (Cmd <$> parseUnique "text" f)
    <*> (UserId <$> parseUnique "user_id" f)

data SlackResponse
  = SlackFail Text
  | SlackSucc Spell

instance ToJSON SlackResponse where
  toJSON (SlackFail text) =
    object
    [ "response_type" .= String "ephemeral"
    , "text" .= text ]
  toJSON (SlackSucc spell) =
    object
      [ "response_type" .= String "in_channel"
      , "attachments" .= [ object
        [ "title" .= name spell
        , "text" .= ("_" <> (typeLine spell) <> "_")
        , "mrkdwn_in" .= (String <$> [ "text", "fields" ])
        , "fields" .= catMaybes
          [ Just (shortField "Casting Time" (castingTime spell))
          , Just (shortField "Range" (range spell))
          , Just (shortField "Components" (components spell))
          , Just (shortField "Duration" (duration spell))
          , Just (longField "Effect" (description spell))
          , (longField "At Higher Levels" <$> (higherLevels spell))
          ]
        ] ] 
      ]

longField :: Text -> Text -> Value
longField t v = object (fieldkvs t v)

shortField :: Text -> Text -> Value
shortField t v = object (("short" .= String "true") : fieldkvs t v)

fieldkvs :: Text -> Text -> [ Pair ]
fieldkvs title value = 
  [ "title" .= title
  , "value" .= value
  ]

errorResponse :: Text -> [ Text ] -> SlackResponse
errorResponse spellName alts = 
  let 
    alternatives =
      if (null alts) then
        ""
      else
        " Possible matches: " <> (intercalate ", " alts)
  in
    SlackFail ("No spell called " <> spellName <> " found." <> alternatives)

api :: [ Spell ] -> PostData -> Handler SlackResponse
api spellbook (PostData (Cmd cmd) _) =
  let
    spellName = strip cmd
  in
    pure $ 
      if (T.null spellName) then 
        SlackFail "I need the name of a spell to look up"
      else 
        either (errorResponse spellName) SlackSucc (findSpell spellbook cmd)