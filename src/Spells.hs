module Spells where

import Data.Aeson
import Data.Text (Text, strip, toCaseFold)
import Data.Text.Metrics (jaro)
import Data.List (find, sortOn)
import Data.Ratio (Ratio, (%))

data Spell = Spell
  { name         :: Text
  , typeLine     :: Text
  , classes      :: [ Text ]
  , castingTime  :: Text
  , range        :: Text
  , components   :: Text
  , duration     :: Text
  , description  :: Text
  , higherLevels :: Maybe Text
  }

instance FromJSON Spell where
  parseJSON = withObject "spell" $ \v -> Spell
    <$> v .: "name"
    <*> v .: "type-line"
    <*> v .: "classes"
    <*> v .: "casting-time"
    <*> v .: "range"
    <*> v .: "components"
    <*> v .: "duration"
    <*> v .: "description"
    <*> v .:? "at-higher-levels"

findSpell :: [ Spell ] -> Text -> Either [ Text ] Spell
findSpell ss t =
  let
    sname = (toCaseFold . strip) t
    spell = find (\s -> toCaseFold (name s) == sname) ss
    alternatives = findSuggestions ss sname
  in
    maybe (Left alternatives) Right spell 

findSuggestions :: [ Spell ] -> Text -> [ Text ]
findSuggestions ss t =
  let
    threshhold = 4 % 5
    possibles = foldr (matchFor threshhold t) [] ss
  in 
    (fmap snd) . (take 5) . reverse . (sortOn fst) $ possibles

matchFor :: Ratio Int -> Text -> Spell -> [ (Ratio Int, Text) ] -> [ (Ratio Int, Text) ]
matchFor threshhold t s ss =
  let
    similarity = jaro (toCaseFold (name s)) t
  in
    if (similarity < threshhold) then ss else (similarity, name s) : ss