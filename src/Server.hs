{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}

module Server
    ( startApp
    ) where

import Data.Aeson (eitherDecode)
import Spells (Spell)
import System.Exit (die)
import Network.Socket (setNonBlockIfNeeded, fdSocket)
import Network.Wai
import Network.Wai.Handler.Warp
import Data.Maybe (isJust)
import Servant
import qualified Slack as Slack
import Network.Wai.Middleware.RequestLogger
import Network.Socket.Activation (getActivatedSockets)
import System.ReadEnvVar
import Paths_spellbot (getDataFileName)
import qualified Data.ByteString.Lazy as B

type API =
  "srd" :> "slack"  :> "spells" :> Slack.API

debug :: IO Bool
debug = -- pure True
 isJust <$> (lookupEnv "DEBUG_MODE" :: IO (Maybe String))

defaultPort :: Int
defaultPort = 1235

settings :: Settings
settings = setPort defaultPort defaultSettings

startApp :: IO ()
startApp =
  let
    runApp spells d =
      if d then logStdoutDev (app spells) else app spells
  in do
    d <- debug
    maybeSocks <- getActivatedSockets
    spells <- loadData
    case maybeSocks of 
      Nothing  -> run defaultPort $ runApp spells d
      Just [s] -> 
        do
          setNonBlockIfNeeded (fdSocket s)
          runSettingsSocket settings s $ runApp spells d
      Just ss  -> die ("Expected one configured socket, got " <> show (length ss))

loadData :: IO [ Spell ]
loadData = do
  file <- getDataFileName "spells/spells.json"
  bs   <- B.readFile file
  either (\s -> die ("Could not parse spellbook: " <> s)) pure (eitherDecode bs)

app :: [ Spell ] -> Application
app spellbook = serve api (Slack.api spellbook)

api :: Proxy API
api = Proxy
