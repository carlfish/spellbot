# Usage: #

## Slack (slash command): ##

Responds to the configured slash command. Errors will be quietly reported
to the requester without bothering the rest of the channel

For example, if configured against the /spell command:

    /spell fireball

## Licensing

All spells served by this bot are from the D&D 5E SRD, made available under the Open Gaming License.
This means that many spells (even ones in the Players Handbook) are missing. There is nothing that
can be done about this without breaking the law, which I would rather not do.