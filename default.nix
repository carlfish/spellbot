{ mkDerivation, aeson, base, bytestring, http-api-data, network
, read-env-var, servant, servant-server, socket-activation, stdenv
, text, text-metrics, wai, wai-extra, warp
}:
mkDerivation {
  pname = "spellbot";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  enableSeparateDataOutput = true;
  libraryHaskellDepends = [
    aeson base bytestring http-api-data network read-env-var servant
    servant-server socket-activation text text-metrics wai wai-extra
    warp
  ];
  executableHaskellDepends = [ base ];
  testHaskellDepends = [ base ];
  homepage = "https://bitbucket.org/carlfish/spellbot";
  license = stdenv.lib.licenses.bsd3;
}
